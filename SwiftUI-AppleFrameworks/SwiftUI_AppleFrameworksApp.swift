//
//  SwiftUI_AppleFrameworksApp.swift
//  SwiftUI-AppleFrameworks
//
//  Created by Tofik Khan on 5/9/21.
//

import SwiftUI

@main
struct SwiftUI_AppleFrameworksApp: App {
    var body: some Scene {
        WindowGroup {
            FrameworkGridView()
        }
    }
}
