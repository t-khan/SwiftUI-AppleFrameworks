//
//  PrimaryButtonUI.swift
//  SwiftUI-AppleFrameworks
//
//  Created by Tofik Khan on 5/10/21.
//

import SwiftUI

struct PrimaryButtonUI: View {
    
    var title: String
    
    var body: some View {
        Text(title)
            .font(.title2)
            .fontWeight(.semibold)
            .frame(width: 280, height: 50)
            .background(Color.red)
            .foregroundColor(Color.white)
            .cornerRadius(10)
    }
}

struct PrimaryButtonUI_Previews: PreviewProvider {
    static var previews: some View {
        PrimaryButtonUI(title: "Hello")
    }
}
